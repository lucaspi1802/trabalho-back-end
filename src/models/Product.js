const DataTypes = require('sequelize');
const sequelize = require('../config/sequelize');

const Product = sequelize.define('Product', {

    name:{
        type: DataTypes.STRING, 
        allowNull: false
    },

    price:{
        type: DataTypes.DOUBLE,
        allowNull: false

    },

    description:{
        type: DataTypes.STRING,
        allowNull: false
    },
     
    photograph:{
        type: DataTypes.STRING,
        alllowNull: false
    },

    evaluation:{
        type: DataTypes.STRING,
        alllowNull: false
    },
});
Product.associate = function(models) {
    Product.belongsTo(models.User, {
        foreignKey: 'sellerId',
        as: 'Seller'
    });
    Product.belongsTo(models.User, {
        foreignKey: 'buyerId',
        as: 'Buyer'
    });

};

module.exports = Product;