const DataTypes = require('sequelize');
const sequelize = require('../config/sequelize');

const User = sequelize.define('User', {

    name:{
        type: DataTypes.STRING,
        allowNull: false
    },

    email:{
        type: DataTypes.STRING,
        allowNull: false
    },

    CPF:{
        type: DataTypes.STRING,
        allowNull: false
    },

    password:{
        type: DataTypes.STRING,
        allowNull: false

    },

    photograph:{
        type: DataTypes.STRING,
    },

});

User.associate = function(models){
    User.hasMany(models.Product, {
        foreignKey: 'sellerId',
        as: 'Seller'
    });
    User.hasOne(models.Product, {
        foreignKey: 'buyerId',
        as: 'Buyer'
    })
    User.hasMany(models.CreditCard);
};

module.exports = User;