const { Router } = require('express');
const ProductController = require('../controllers/ProductController');
const UserController = require('../controllers/UserController');
const CreditCardController = require('../controllers/CreditCardController');
const router = Router();

router.get('/products', ProductController.index);
router.get('/products/:id', ProductController.show);
router.post('/products', ProductController.create);
router.put('/products/:id', ProductController.update);
router.delete('/products/:id', ProductController.destroy);
router.put('/product/productaddbuyer/:id', ProductController.addBuyer);
router.delete('/products/productremovebuyer/:id', ProductController.removeBuyer);
router.put('/product/productaddseller/:id', ProductController.addSeller);
router.delete('/products/productremoveseller/:id', ProductController.removeSeller);


router.get('/users', UserController.index);
router.get('/users/:id', UserController.show);
router.post('/users', UserController.create);
router.put('/users/:id', UserController.update);
router.delete('/users/:id', UserController.destroy);

router.get('/creditcards', CreditCardController.index);
router.get('/creditcards/:id', CreditCardController.show);
router.post('/creditcards', CreditCardController.create);
router.put('/creditcards/:id', CreditCardController.update);
router.delete('/creditcards/:id', CreditCardController.destroy);
router.put('/creditcards/addrole/:id', CreditCardController.addRelationship);
router.delete('/products/addrole/:id', CreditCardController.removeRellationship);

module.exports = router;
